#!/bin/bash

set -eu

if [[ ! -f /app/data/config.php ]]; then
    echo "=> Detected first run"

    mkdir -p /app/data/logs

    # copy stuff
    cp -r /app/code/.htaccess_orig /app/data/htaccess
    cp /app/pkg/cloudron_config.php /app/data/config.php
fi

mkdir -p /run/phpsm/sessions


# update settings in case of changes
if [ $(mysql -N -s --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE} -e \
    "select count(*) from monitor_config;") -ge 1 ]; then

	echo "updating stuff in the database!"
	readonly mysql_cli="mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} -P ${CLOUDRON_MYSQL_PORT} ${CLOUDRON_MYSQL_DATABASE}"

	# this is not working yet, you have to save the password manually at the moment
	SMTP_PASS_ENC=`echo -n ${CLOUDRON_MAIL_SMTP_PASSWORD} | openssl dgst -sha256 -binary | openssl base64`

	# set up email - not working because it has to be encrypted beforehand
	$mysql_cli -e "UPDATE monitor_config SET value = '1' WHERE monitor_config.key = 'email_status';";
	$mysql_cli -e "UPDATE monitor_config SET value = '${CLOUDRON_MAIL_SMTP_USERNAME}' WHERE monitor_config.key = 'email_from_email';";
	$mysql_cli -e "UPDATE monitor_config SET value = '1' WHERE monitor_config.key = 'email_smtp';";
	$mysql_cli -e "UPDATE monitor_config SET value = '${CLOUDRON_MAIL_SMTP_SERVER}' WHERE monitor_config.key = 'email_smtp_host';";
	$mysql_cli -e "UPDATE monitor_config SET value = '1' WHERE monitor_config.key = 'email_smtp';";
	$mysql_cli -e "UPDATE monitor_config SET value = '${CLOUDRON_MAIL_SMTP_PORT}' WHERE monitor_config.key = 'email_smtp_port';";
	$mysql_cli -e "UPDATE monitor_config SET value = '' WHERE monitor_config.key = 'email_smtp_security';";
	$mysql_cli -e "UPDATE monitor_config SET value = '${CLOUDRON_MAIL_SMTP_USERNAME}' WHERE monitor_config.key = 'email_smtp_username';";
	$mysql_cli -e "UPDATE monitor_config SET value = '${SMTP_PASS_ENC}' WHERE monitor_config.key = 'email_smtp_password';";

	# set up ldap
	$mysql_cli -e "UPDATE monitor_config SET value = 'openldap' WHERE monitor_config.key = 'authdir_type';";
	$mysql_cli -e "UPDATE monitor_config SET value = '1' WHERE monitor_config.key = 'dirauth_status';";
	$mysql_cli -e "UPDATE monitor_config SET value = '1' WHERE monitor_config.key = 'authdir_ldapfollowref';";
	$mysql_cli -e "UPDATE monitor_config SET value = '${CLOUDRON_LDAP_HOST}' WHERE monitor_config.key = 'authdir_host_locn';";
	$mysql_cli -e "UPDATE monitor_config SET value = '${CLOUDRON_LDAP_PORT}' WHERE monitor_config.key = 'authdir_host_port';";
	$mysql_cli -e "UPDATE monitor_config SET value = '3' WHERE monitor_config.key = 'authdir_ldapver';";
	$mysql_cli -e "UPDATE monitor_config SET value = 'dc=cloudron' WHERE monitor_config.key = 'authdir_basedn';";
	$mysql_cli -e "UPDATE monitor_config SET value = 'username' WHERE monitor_config.key = 'authdir_usernameattrib';";
	$mysql_cli -e "UPDATE monitor_config SET value = 'ou=users' WHERE monitor_config.key = 'authdir_usercontainerrdn';";
	$mysql_cli -e "UPDATE monitor_config SET value = 'ou=groups' WHERE monitor_config.key = 'authdir_groupcontainerrdn';";
else
    echo "Finish installation first!"
fi



echo "=> Ensure permissions"
chown -R www-data.www-data /app/data

echo "=> Run PHP Server Monitor"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"

exec /usr/sbin/apache2 -DFOREGROUND
