FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4

WORKDIR /app/code

ARG VERSION=3.6.0

# get phpservermon
RUN wget https://github.com/viharm/phpservermon/archive/feature-ldapauth.zip && \
    unzip feature-ldapauth.zip && \
    rm feature-ldapauth.zip

# move folders around
RUN mv /app/code/phpservermon-feature-ldapauth/* /app/code/phpservermon-feature-ldapauth/.htaccess /app/code/ && \
    rm -R /app/code/phpservermon-feature-ldapauth/ && \
    rm -R /app/code/logs && \
    mv /app/code/.htaccess /app/code/.htaccess_orig
 
# link to static directories
RUN ln -sf /app/data/htaccess /app/code/.htaccess && \
    ln -sf /app/data/config.php /app/code/config.php && \
    ln -sf /app/data/logs /app/code/logs

RUN composer update && composer install

RUN mkdir -p /run/phpsm/sessions

RUN chown -R www-data.www-data /app/code /run

# configure apache
RUN rm /etc/apache2/sites-enabled/* && \
    sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
COPY apache/phpservermon.conf /etc/apache2/sites-enabled/phpservermon.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

# configure mod_php
RUN a2enmod php7.3 rewrite
RUN crudini --set /etc/php/7.3/apache2/php.ini PHP upload_max_filesize 128M && \
    crudini --set /etc/php/7.3/apache2/php.ini PHP upload_max_size 128M && \
    crudini --set /etc/php/7.3/apache2/php.ini PHP post_max_size 128M && \
    crudini --set /etc/php/7.3/apache2/php.ini PHP memory_limit 256M && \
    crudini --set /etc/php/7.3/apache2/php.ini PHP max_execution_time 200 && \
    crudini --set /etc/php/7.3/apache2/php.ini Session session.save_path /run/phpsm/sessions && \
    crudini --set /etc/php/7.3/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.3/apache2/php.ini Session session.gc_divisor 100

ADD start.sh cloudron_config.php /app/pkg/

CMD [ "/app/pkg/start.sh" ]
