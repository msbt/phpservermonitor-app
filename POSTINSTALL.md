This application integrates with cloudron, but only for regular users (this can be changed in the config after the setup though).

There is no way to finish the installation via cli, this means you have to create an admin-user to finish it up. Navigate to the config screen and authentication, check the `Authenticate with directory service` box and restart the app - only after the restart the SMTP and LDAP settings are written to the database.

By default, the cron-scheduler runs every 15 minutes or if you trigger it manually from PSM.
